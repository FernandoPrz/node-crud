'use strict';

require('./src/config/index.js');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const corsOptions = {
  origin: 'http://localhost:3000'
};
app.use(cors(corsOptions));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());


// Rutas de la aplicacion
app.get('/', (req, res) => res.json('Hola mundo') );
require('./src/routes/activityRoutes')(app);
 
app.listen(process.env.PORT, ()=> console.log(`Iniciado en puerto: ${process.env.PORT}`) );