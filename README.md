hola mundo 0
1. sequelize-cli https://github.com/sequelize/cli
  - Instalar sequelize-cli ```npm install --save-dev sequelize-cli```
  - Ejecutarlo dentro de .\src\ ```npx sequelize init```
  - Instalar el parquete para el [dialecto](https://github.com/sequelize/sequelize-auto#prerequisites) ```npm install -g mysql``` o ```npm install -g mssql```

2. Convertir src\config\config.json a config.js
  ```js
    'use strict';

    module.exports = {
      "development": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS ? process.env.DB_PASS : null,
        "database": process.env.DB_NAME,
        "host"    : process.env.DB_HOST,
        "dialect" : process.env.DB_DIALECT,
        "operatorsAliases": false
      },
      "test": {
        // ...
      },
      "production": {
        //...
      }
    }

  ```

3. Cambiar la ruta de del config.js en src/models/index.js
  ```js
  'use strict';

  const fs = require('fs');
  const path = require('path');
  const Sequelize = require('sequelize');
  const basename = path.basename(__filename);
  const env = process.env.NODE_ENV || 'development';
  //const config = require(__dirname + '/../config/config.json')[env];
  const config = require(__dirname + '/../config/config.js')[env];

  // ...
  ```

4. Establecer las env para ser accedidas en 'src/config/config.js' en process.env.DB_*
  - Instalar dotenv ```npm install --save dotenv```
  - Crear archivo ./.env
  ```
    DB_HOST=localhost
    DB_PORT=
    DB_NAME=database
    DB_USER=rootss
    DB_PASS=s1mpl3
    DB_DIALECT=mysql

    DB_MSSQL_DIALECT=mssql
  ```
5. Crear los modelos con [sequelize-auto](https://github.com/sequelize/sequelize-auto) si esta definida la base de datos
  - Instalar sequelize-auto ```npm install -g sequelize-auto```
  - Instalar los prerequisitos ```npm install -g mysql``` o ```npm install -g mssql```
  - Obtener generar automaticamente los modelos en ./src/models```sequelize-auto -o "./src/models" -d ucaribelabs -h 127.0.0.1 -u root -x my_password -p 3306 -e mysql ``` omitir -x si no hay contraseña

6. Establecer las relaciones [(associations)](https://sequelize.org/master/manual/associations.html). entre modelos 
  
  ```js
    // users.js
    /* jshint indent: 2 */
    module.exports = function(sequelize, DataTypes) {
      const users = sequelize.define('users', {
        id: {
          type: DataTypes.INTEGER(11),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
        },
        // ...
      }, {
        tableName: 'users'
      });
      
      // --------- Aqui se definen ------------
      // En este apartado definimos las relaciones del modelo
        ucaribe_users.associate = function(models) {
          models.ucaribe_users.belongsTo(models.usertypes, { foreignKey: 'userTypeID' });
          models.ucaribe_users.belongsTo(models.majors, { foreignKey: 'majorID' });
        }

      return users;
    };
  ```

  ```js
    // userstypes.js
    /* jshint indent: 2 */

    module.exports = function(sequelize, DataTypes) {
      const usertypes = sequelize.define('usertypes', {
        id: {
          type: DataTypes.INTEGER(11),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
        },
        // ...
      }, {
        tableName: 'usertypes'
      });

      // En este apartado definimos las relaciones del modelo
      usertypes.associate = function(models) {
        models.usertypes.hasMany(models.ucaribe_users, { foreignKey: 'userTypeID' });
      }
      return usertypes;
    };

  ```

7. Crear el CRUD 
  - crear carpetas src/routes src/controllers
  - Dentro de cada carpeta crear.
  Crear el controlador:
  ```js
    'use strict';

    const models = require('./../models/index');
    const Op = models.Sequelize.Op;

    const Activity = models.activities;

    // Create and Save a new item
    exports.create = (req, res) => {
      // ...
    };

    // Retrieve all items from the database.
    exports.findAll = (req, res) => {
      // ...
    };

    // Find a single item with an id
    exports.findOne = (req, res) => {
      // ...
    };

    // Update a item by the id in the request
    exports.update = (req, res) => {
      // ...
    };

    // Delete a item with the specified id in the request
    exports.delete = (req, res) => {
      // ...
    };

    // Find all published items
    exports.findAllPublished = (req, res) => {
      // ...
    };
  ```
  Crear las rutas:
  ```js
    // src/routes/activityRoutes.js
    const router = require('express').Router();
    const activityController = require('./../controllers/activityController.js');

    module.exports = (app) => {
      // Create a new Activity
      router.post('/', activityController.create);

      // Retrieve all Activities
      router.get('/', activityController.findAll);

      // Retrieve all published Activities
      router.get('/published', activityController.findAllPublished);

      // Retrieve a single Activity with id
      router.get('/:id', activityController.findOne);

      // Update a Activity with id
      router.put('/:id', activityController.update);

      // Delete a Activity with id
      router.delete('/:id', activityController.delete);

      app.use('/api/activity', router);
    };
  ```
  Añadir las rutas de activity dentro de server.js
  ```js
    'use strict';

    require('./src/config/index.js');

    const express = require('express');
    const bodyParser = require('body-parser');
    const cors = require('cors');
    const app = express();

    const corsOptions = {
      origin: 'http://localhost:3000'
    };
    app.use(cors(corsOptions));
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: false }));
    // parse application/json
    app.use(bodyParser.json());


    // Rutas de la aplicacion
    app.get('/', (req, res) => res.json('Hola mundo') );
    require('./src/routes/activityRoutes')(app);
    
    app.listen(process.env.PORT, ()=> console.log(`Iniciado en puerto: ${process.env.PORT}`) );
  ```

8. Para postman poner en headers:
  - Accept        application/json
  - Content-Type  application/json