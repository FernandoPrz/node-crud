'use strict';

require('./../src/config/index.js');

const models = require('./../src/models/index');
const Op = require('sequelize').Op;

models.activities.findAll().then(response => {
  console.log(JSON.stringify(response, null, 2));
});


models.ucaribe_users.findAll({
  include: [
    { model: models.majors },
    { model: models.usertypes }
  ]
}).then(response => {
  console.log(JSON.stringify(response, null, 2));
});


models.laboratories.findAll({
  include: [
    { model: models.ucaribe_users },
  ]
}).then(response => {
  console.log(JSON.stringify(response, null, 2));
});


models.laboratories_has_activities.findAll({
  attributes: ['id', 'activityID', 'laboratoryID'],
  where: {
    // activityID: 1,
    laboratoryID: {
      // [Op.between]: [4, 6]
      [Op.notBetween]: [4, 6]
    }
  },
  include: [{
      model: models.activities,
      attributes: ['id', 'name'],
      // where: { id: 1 }
    },{
      model: models.laboratories,
      attributes: ['id', 'name'],
      // where: { id: 5 }
    },
  ]
}).then(response => {
  console.log(JSON.stringify(response, null, 2));
});