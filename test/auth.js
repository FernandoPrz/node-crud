'use strict';

require('./../src/config/index.js');

const models = require('./../src/models/index');

models.sequelize.authenticate()
  .then(() => console.log('Connection has been established successfully.') )
  .catch(err => console.error('Unable to connect to the database:', err) );
