/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('vouchers_has_tools', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    voucherID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'vouchers',
        key: 'id'
      }
    },
    laboratoryHasToolID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    delivered: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'vouchers_has_tools'
  });
};
