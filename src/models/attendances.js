/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('attendances', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    startDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    endDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ucaribeUserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'ucaribe_users',
        key: 'id'
      }
    },
    laboratoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    activityID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'activities',
        key: 'id'
      }
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATEONLY,
      allowNull: false
    }
  }, {
    tableName: 'attendances'
  });
};
