/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const activities = sequelize.define('activities', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'activities'
  });

  activities.associate = function(models){
    models.activities.hasMany(models.laboratories_has_activities, { foreignKey: 'activityID' } );
  }
  
  return activities;
};
