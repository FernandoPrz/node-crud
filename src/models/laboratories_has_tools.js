/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('laboratories_has_tools', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    laboratoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'laboratories',
        key: 'id'
      }
    },
    toolID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'tools',
        key: 'id'
      }
    },
    quantity: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'laboratories_has_tools'
  });
};
