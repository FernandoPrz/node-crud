/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const majors = sequelize.define('majors', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'majors'
  });

  // En este apartado definimos las relaciones del modelo
  majors.associate = function(models) {
    models.majors.hasMany(models.ucaribe_users, { foreignKey: 'majorID' });
  }

  return majors;
};
