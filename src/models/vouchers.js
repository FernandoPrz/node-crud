/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('vouchers', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    folio: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    completed: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    ucaribeUserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'ucaribe_users',
        key: 'id'
      }
    },
    laboratoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'laboratories',
        key: 'id'
      }
    },
    startDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    createdBy: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'vouchers'
  });
};
