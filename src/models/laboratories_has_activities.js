/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const laboratories_has_activities = sequelize.define('laboratories_has_activities', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    laboratoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'laboratories',
        key: 'id'
      }
    },
    activityID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'activities',
        key: 'id'
      }
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'laboratories_has_activities'
  });

  laboratories_has_activities.associate = function(models) {
    models.laboratories_has_activities.belongsTo(models.activities,  { foreignKey: 'activityID' });
    models.laboratories_has_activities.belongsTo(models.laboratories,  { foreignKey: 'laboratoryID' });
  }

  return laboratories_has_activities;
};
