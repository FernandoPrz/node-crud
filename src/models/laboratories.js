/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const laboratories = sequelize.define('laboratories', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    shortName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ucaribeUserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'ucaribe_users',
        key: 'id'
      }
    }
  }, {
    tableName: 'laboratories'
  });

  laboratories.associate = function(models) {
    models.laboratories.belongsTo(models.ucaribe_users, { foreignKey: 'ucaribeUserID' });
    models.laboratories.hasMany(models.laboratories_has_activities, { foreignKey: 'laboratoryID' });
  }

  return laboratories;
};
