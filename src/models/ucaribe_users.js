/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const ucaribe_users = sequelize.define('ucaribe_users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    realm: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    emailVerified: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    verificationToken: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    enrollment: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    firstname: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    lastname: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(512),
      allowNull: false
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    majorID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    userTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'ucaribe_users'
  });

  // En este apartado definimos las relaciones del modelo
  ucaribe_users.associate = function(models) {
    models.ucaribe_users.belongsTo(models.usertypes, { foreignKey: 'userTypeID' });
    models.ucaribe_users.belongsTo(models.majors, { foreignKey: 'majorID' });
    models.ucaribe_users.hasMany(models.laboratories, { foreignKey: 'ucaribeUserID' });
  }

  return ucaribe_users;
};
