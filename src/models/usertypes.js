/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const usertypes = sequelize.define('usertypes', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    type: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'usertypes'
  });

  // En este apartado definimos las relaciones del modelo
  usertypes.associate = function(models) {
    models.usertypes.hasMany(models.ucaribe_users, { foreignKey: 'userTypeID' });
  }

  return usertypes;
};
