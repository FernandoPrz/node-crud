'use strict';

const models = require('./../models/index');
const Op = models.Sequelize.Op;

const Activity = models.activities;

// Create and Save a new item
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: 'El contenido no puede estar vacio' });
    return;
  }

  // Create a item
  const activity = {
    name: req.body.name,
    description: req.body.description,
    state: req.body.state ? req.body.state : false
  };

  // Save item in the database
  Activity.create(activity)
    .then(data => res.send(data))
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Ocurrio un error intentando crear el item.'
      });
    });
};





// Retrieve all items from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  Activity.findAll({ where: condition })
    .then(data => res.send(data))
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Ocurrio un error intentando obtener los item.'
      });
    });
};





// Find a single item with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Activity.findByPk(id)
    .then(data => res.send(data))
    .catch(err => {
      res.status(500).send({
        message: `Error: No se ha podido conseguir el item con el id ${id}`
      });
    });
};





// Update a item by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Activity.update(req.body, { where: { id: id } })
    .then(num => {
      if (num == 1) {
        res.send({ message: 'Se ha actualizado el item correctamente.' });
      } else {
        res.send({ message: `Ocurrio un error intentando actualizar el item=${id}. No pudo haberse encontrado o req.body puede estar vacio` });
      }
    })
    .catch(err => {
      res.status(500).send({ message: `Ocurrio un error intentando actualizar el item=${id}.` });
    });
};

// Delete a item with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Activity.destroy({ where: { id: id } })
    .then(num => {
      if (num == 1) {
        res.send({ message: `Se ha eliminado el item con id=${id}` });
      } else {
        res.send({ message: `No se ha podido eliminar el item con id=${id}. No se ha encontrado el elemento con el id.` });
      }
    })
    .catch(err => {
      res.status(500).send({ message: `No se ha podido eliminar el item con id=${id}.` });
    });
};


// Find all published items
exports.findAllPublished = (req, res) => {
  Activity.findAll({ where: { state: true } })
    .then(data => { res.send(data) })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Ocurrio un error intentando obtener los items activoss'
      });
    });
};