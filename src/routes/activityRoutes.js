const router = require('express').Router();
const activityController = require('./../controllers/activityController.js');

module.exports = (app) => {
  // Create a new Activity
  router.post('/', activityController.create);

  // Retrieve all Activities
  router.get('/', activityController.findAll);

  // Retrieve all published Activities
  router.get('/published', activityController.findAllPublished);

  // Retrieve a single Activity with id
  router.get('/:id', activityController.findOne);

  // Update a Activity with id
  router.put('/:id', activityController.update);

  // Delete a Activity with id
  router.delete('/:id', activityController.delete);

  app.use('/api/activity', router);
};