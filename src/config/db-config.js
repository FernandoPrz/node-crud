'use strict';

module.exports = {
  "development": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS ? process.env.DB_PASS : null,
    "database": process.env.DB_NAME,
    "host"    : process.env.DB_HOST,
    "dialect" : process.env.DB_DIALECT,
    "operatorsAliases": "0",
    "define": {
      "timestamps": false
    }
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "operatorsAliases": "0",
    "define": {
      "timestamps": false
    }
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "operatorsAliases": "0",
    "define": {
      "timestamps": false
    }
  }
}
