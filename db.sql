-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 14-01-2020 a las 09:37:32
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ucaribelabs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `activities`
--

INSERT INTO `activities` (`id`, `name`, `description`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 'Clases', '', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(2, 'Asesoría / Tutoría', '', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(3, 'Trabajo en equipo', '', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(4, 'Otros', '', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(5, 'Equipo de computo', '', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(6, 'Equipo', '', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attendances`
--

CREATE TABLE `attendances` (
  `id` int(11) NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `ucaribeUserID` int(11) NOT NULL,
  `laboratoryID` int(11) NOT NULL,
  `activityID` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `attendances`
--

INSERT INTO `attendances` (`id`, `startDate`, `endDate`, `ucaribeUserID`, `laboratoryID`, `activityID`, `state`, `createdAt`, `updatedAt`) VALUES
(1, '2019-07-13 00:00:00', '2019-07-13 00:00:01', 1, 0, 1, 1, '2019-07-13', '2019-07-13'),
(2, NULL, NULL, 2, 0, 2, 1, '2019-07-13', '2019-07-13'),
(3, NULL, NULL, 1, 1, 1, 1, '2020-01-10', '2020-01-10'),
(4, NULL, NULL, 1, 5, 2, 1, '2020-01-10', '2020-01-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `majors`
--

CREATE TABLE `majors` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `majors`
--

INSERT INTO `majors` (`id`, `name`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 'Licenciatura en Gastronomía', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(2, 'Licenciatura en Negocios Internacionales', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(3, 'Licenciatura en Innovación Empresarial', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(4, 'Licenciatura en Turismo Sustentable y Gestión Hotelera', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(5, 'Ingeniería Industrial', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(6, 'Ingeniería Telemática', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(7, 'Ingeniería en Logística y Cadena de Suministro', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(8, 'Ingeniería en Datos e Inteligencia Organizacional', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(9, 'Ingeniería HAmbiental', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratories`
--

CREATE TABLE `laboratories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` mediumtext NOT NULL,
  `shortName` varchar(45) DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `ucaribeUserID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `laboratories`
--

INSERT INTO `laboratories` (`id`, `name`, `password`, `shortName`, `state`, `createdAt`, `updatedAt`, `ucaribeUserID`) VALUES
(1, 'Laboratorio Telemática', 'Lorem', 'LT', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 1),
(2, 'Laboratorio de Hardware y Cableado', 'Lorem', 'LHC', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 1),
(3, 'Laboratorio Multimedia', 'Lorem', 'LMUL', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 1),
(4, 'Laboratorio de Manufactura', 'Lorem', 'LAMA', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 1),
(5, 'Laboratorio de Mecanica', 'Lorem', 'LAME', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 2),
(6, 'Laboratorio de Tecnologías de la Información', 'Lorem', 'LTI', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratories_has_activities`
--

CREATE TABLE `laboratories_has_activities` (
  `id` int(11) NOT NULL,
  `laboratoryID` int(11) NOT NULL,
  `activityID` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `laboratories_has_activities`
--

INSERT INTO `laboratories_has_activities` (`id`, `laboratoryID`, `activityID`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 5, 1, 1, '2019-07-14 00:00:00', '2019-07-14 00:00:00'),
(2, 5, 2, 1, '2019-07-14 00:00:00', '2019-07-14 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratories_has_tools`
--

CREATE TABLE `laboratories_has_tools` (
  `id` int(11) NOT NULL,
  `laboratoryID` int(11) NOT NULL,
  `toolID` int(11) NOT NULL,
  `quantity` float DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `laboratories_has_tools`
--

INSERT INTO `laboratories_has_tools` (`id`, `laboratoryID`, `toolID`, `quantity`, `description`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 5, 1, 3, NULL, 0, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(2, 5, 2, 4, NULL, 0, '2019-01-01 00:00:00', '2019-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tools`
--

CREATE TABLE `tools` (
  `id` int(11) NOT NULL,
  `enrollment` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tools`
--

INSERT INTO `tools` (`id`, `enrollment`, `name`, `description`, `state`, `createdAt`, `updatedAt`) VALUES
(1, '0001', 'Multimetro', NULL, 1, '2019-07-13 00:00:00', '2019-07-13 00:00:00'),
(2, '0002', 'Cautin', NULL, 1, '2019-07-13 00:00:00', '2019-07-13 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ucaribe_users`
--

CREATE TABLE `ucaribe_users` (
  `id` int(11) NOT NULL,
  `realm` varchar(512) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `emailVerified` tinyint(1) DEFAULT NULL,
  `verificationToken` varchar(512) DEFAULT NULL,
  `enrollment` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(512) NOT NULL,
  `password` mediumtext DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `state` tinyint(1) NOT NULL,
  `majorID` int(11) DEFAULT NULL,
  `userTypeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ucaribe_users`
--

INSERT INTO `ucaribe_users` (`id`, `realm`, `username`, `emailVerified`, `verificationToken`, `enrollment`, `firstname`, `lastname`, `email`, `password`, `createdAt`, `updatedAt`, `state`, `majorID`, `userTypeID`) VALUES
(1, NULL, NULL, NULL, NULL, '140300050', 'Fernando', 'Pérez Gómez', '140300050@ucaribe.edu.mx', '$2a$10$w3gu1qS3a3mmTi7jB1TGi.kbnxnqNtnfrSNny7LCz9jeosz4CjKky', '2019-07-13 03:43:15', '2019-07-13 03:43:15', 1, NULL, NULL),
(2, NULL, NULL, 0, NULL, 'jvirgen', 'Jarmen Said', 'Virgen Suarez', 'jvirgen@ucaribe.edu.mx', '$2a$10$MkdLHZrwrlQsR8GkI1eZ7e1X6ujM4j4xMxKXm1M0mvyqMiq.oEyEe', '2019-07-14 02:55:20', '2019-07-14 02:55:20', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usertypes`
--

CREATE TABLE `usertypes` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usertypes`
--

INSERT INTO `usertypes` (`id`, `type`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 'Administrador', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(2, 'Responsable de laboratorio', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(3, 'Invitado', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(4, 'Docente', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(5, 'Personal de apoyo', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00'),
(6, 'Estudiante', 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `folio` mediumtext NOT NULL,
  `completed` tinyint(4) NOT NULL,
  `ucaribeUserID` int(11) NOT NULL,
  `laboratoryID` int(11) NOT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vouchers`
--

INSERT INTO `vouchers` (`id`, `folio`, `completed`, `ucaribeUserID`, `laboratoryID`, `startDate`, `endDate`, `createdBy`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 'voucher001', 0, 1, 5, '2019-07-13 00:00:00', NULL, NULL, 1, '2019-07-13 00:00:00', '2019-07-13 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vouchers_has_tools`
--

CREATE TABLE `vouchers_has_tools` (
  `id` int(11) NOT NULL,
  `voucherID` int(11) NOT NULL,
  `laboratoryHasToolID` int(11) NOT NULL,
  `delivered` tinyint(4) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vouchers_has_tools`
--

INSERT INTO `vouchers_has_tools` (`id`, `voucherID`, `laboratoryHasToolID`, `delivered`, `state`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, 0, 1, '2019-07-13 00:00:00', '2019-07-13 00:00:00'),
(2, 1, 2, 0, 1, '2019-07-13 00:00:00', '2019-07-13 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_attendances_users1_idx` (`ucaribeUserID`),
  ADD KEY `fk_attendances_activities1_idx` (`activityID`),
  ADD KEY `fk_attendances_laboratories1_idx` (`laboratoryID`);

--
-- Indices de la tabla `majors`
--
ALTER TABLE `majors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `laboratories`
--
ALTER TABLE `laboratories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_laboratories_users1_idx` (`ucaribeUserID`);

--
-- Indices de la tabla `laboratories_has_activities`
--
ALTER TABLE `laboratories_has_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_laboratories_has_activities_laboratories1_idx` (`laboratoryID`),
  ADD KEY `fk_laboratories_has_activities_activities1_idx` (`activityID`);

--
-- Indices de la tabla `laboratories_has_tools`
--
ALTER TABLE `laboratories_has_tools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_laboratories_has_tools_laboratories1_idx` (`laboratoryID`),
  ADD KEY `fk_laboratories_has_tools_tools1_idx` (`toolID`);

--
-- Indices de la tabla `tools`
--
ALTER TABLE `tools`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ucaribe_users`
--
ALTER TABLE `ucaribe_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_majors_idx` (`majorID`),
  ADD KEY `fk_users_usertypes1_idx` (`userTypeID`);

--
-- Indices de la tabla `usertypes`
--
ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vouchers_users1_idx` (`ucaribeUserID`),
  ADD KEY `fk_vouchers_laboratories1_idx` (`laboratoryID`),
  ADD KEY `fk_vouchers_users2_idx` (`createdBy`);

--
-- Indices de la tabla `vouchers_has_tools`
--
ALTER TABLE `vouchers_has_tools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vouchers_has_tools_vouchers1_idx` (`voucherID`),
  ADD KEY `fk_vouchers_has_tools_laboratories_has_tools1_idx` (`laboratoryHasToolID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `majors`
--
ALTER TABLE `majors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `laboratories`
--
ALTER TABLE `laboratories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `laboratories_has_activities`
--
ALTER TABLE `laboratories_has_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `laboratories_has_tools`
--
ALTER TABLE `laboratories_has_tools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tools`
--
ALTER TABLE `tools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ucaribe_users`
--
ALTER TABLE `ucaribe_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usertypes`
--
ALTER TABLE `usertypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `vouchers_has_tools`
--
ALTER TABLE `vouchers_has_tools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `attendances`
--
ALTER TABLE `attendances`
  ADD CONSTRAINT `fk_attendances_activities1` FOREIGN KEY (`activityID`) REFERENCES `activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_attendances_users1` FOREIGN KEY (`ucaribeUserID`) REFERENCES `ucaribe_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `laboratories`
--
ALTER TABLE `laboratories`
  ADD CONSTRAINT `fk_laboratories_users1` FOREIGN KEY (`ucaribeUserID`) REFERENCES `ucaribe_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `laboratories_has_activities`
--
ALTER TABLE `laboratories_has_activities`
  ADD CONSTRAINT `fk_laboratories_has_activities_activities1` FOREIGN KEY (`activityID`) REFERENCES `activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_laboratories_has_activities_laboratories1` FOREIGN KEY (`laboratoryID`) REFERENCES `laboratories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `laboratories_has_tools`
--
ALTER TABLE `laboratories_has_tools`
  ADD CONSTRAINT `fk_laboratories_has_tools_laboratories1` FOREIGN KEY (`laboratoryID`) REFERENCES `laboratories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_laboratories_has_tools_tools1` FOREIGN KEY (`toolID`) REFERENCES `tools` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vouchers`
--
ALTER TABLE `vouchers`
  ADD CONSTRAINT `fk_vouchers_laboratories1` FOREIGN KEY (`laboratoryID`) REFERENCES `laboratories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vouchers_users1` FOREIGN KEY (`ucaribeUserID`) REFERENCES `ucaribe_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vouchers_has_tools`
--
ALTER TABLE `vouchers_has_tools`
  ADD CONSTRAINT `fk_vouchers_has_tools_vouchers1` FOREIGN KEY (`voucherID`) REFERENCES `vouchers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;